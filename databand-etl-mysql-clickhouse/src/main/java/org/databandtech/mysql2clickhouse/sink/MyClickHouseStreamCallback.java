package org.databandtech.mysql2clickhouse.sink;

import java.io.IOException;

import org.apache.flink.api.java.tuple.Tuple5;

import ru.yandex.clickhouse.util.ClickHouseRowBinaryStream;
import ru.yandex.clickhouse.util.ClickHouseStreamCallback;


public class MyClickHouseStreamCallback implements ClickHouseStreamCallback{

	Tuple5<String, Integer, String, String, String> value;
	public MyClickHouseStreamCallback(Tuple5<String, Integer, String, String, String> value) {
		this.value = value;
	}

	@Override
	public void writeTo(ClickHouseRowBinaryStream stream) throws IOException {
		
		for (int i = 0; i < 10; i++) {
            stream.writeInt32(i);
            stream.writeString(value.f0);
            stream.writeString(value.f2);
            stream.writeString(value.f3);
        }
	}

}
