package org.databandtech.job.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

import org.databandtech.job.entity.ScheduledTaskJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandExecuteJob implements ScheduledTaskJob{
	
	String key;	
	String cmd;	
	
	String location;
	String cron;
	public CommandExecuteJob(String key, String cmd, String location, String cron) {
		super();
		this.key = key;
		this.cmd = cmd;
		this.location = location;
		this.cron = cron;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandExecuteJob.class);

	@Override
	public void run() {
		Process p;
		String cmd = this.cmd;
		String location = this.location;
		
		try {
			//执行命令
			LOGGER.info("CommandExecuteJob => {}  run  当前线程名称 {} ", key, Thread.currentThread().getName());
			p = Runtime.getRuntime().exec(cmd);
			//获取输出流，并包装到BufferedReader中
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(),"GB2312"));
			String line = null;
			while((line = br.readLine()) != null) {
				LOGGER.info("CommandExecuteJob => {} ", line);

			}
			int exitValue = p.waitFor();
			LOGGER.info("进程返回值：\" => {} ", exitValue);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
