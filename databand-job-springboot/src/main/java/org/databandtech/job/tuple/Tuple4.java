package org.databandtech.job.tuple;

import java.util.Optional;

public class Tuple4<E, T, F, G> {
	private E e;
	private T t;
	private F f;
	private G g;

	Tuple4(E e, T t, F f, G g) {
		this.e = e;
		this.t = t;
		this.f = f;
		this.g = g;
	}

	public Optional<E> _1() {
		return Optional.of(e);
	}

	public Optional<T> _2() {
		return Optional.of(t);
	}

	public Optional<F> _3() {
		return Optional.of(f);
	}

	public Optional<G> _4() {
		return Optional.of(g);
	}

}
