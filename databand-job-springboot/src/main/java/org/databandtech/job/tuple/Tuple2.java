package org.databandtech.job.tuple;

import java.util.Optional;

public class Tuple2<E, T> {
	private E e;
	private T t;

	Tuple2(E e, T t) {
		this.e = e;
		this.t = t;
	}

	public Optional<E> _1() {
		return Optional.of(e);
	}

	public Optional<T> _2() {
		return Optional.of(t);
	}

}
