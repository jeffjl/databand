package org.databandtech.job.tuple;

import java.util.Optional;

public class Tuple3<E, T, F> {
	private E e;
	private T t;
	private F f;

	Tuple3(E e, T t, F f) {
		this.e = e;
		this.t = t;
		this.f = f;
	}

	public Optional<E> _1() {
		return Optional.of(e);
	}

	public Optional<T> _2() {
		return Optional.of(t);
	}

	public Optional<F> _3() {
		return Optional.of(f);
	}

}
