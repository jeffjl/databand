package org.databandtech.job.entity;

import org.databandtech.job.sink.MysqlSink;

public interface SavableTaskJob<T> {
	
	public String SaveData(T t, MysqlSink dest); 

}
