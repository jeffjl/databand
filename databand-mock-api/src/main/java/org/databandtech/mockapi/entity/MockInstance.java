package org.databandtech.mockapi.entity;

import java.util.Map;

import org.mockserver.model.Parameter;

public class MockInstance {

	String describe;
	String method;
	String path;
	Parameter[] path_parameters;
	Parameter[] query_string_parameters; 
	Map<String,String>  req_cookie;
	Map<String,String>  req_headers;
	String req_jsonbody;
	int resp_statuscode;
	Map<String,String>  resp_cookie;
	Map<String,String>  resp_headers;
	String resp_body;
	
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Parameter[] getPath_parameters() {
		return path_parameters;
	}
	public void setPath_parameters(Parameter[] path_parameters) {
		this.path_parameters = path_parameters;
	}
	public Parameter[] getQuery_string_parameters() {
		return query_string_parameters;
	}
	public void setQuery_string_parameters(Parameter[] query_string_parameters) {
		this.query_string_parameters = query_string_parameters;
	}
	public Map<String, String> getReq_cookie() {
		return req_cookie;
	}
	public void setReq_cookie(Map<String, String> req_cookie) {
		this.req_cookie = req_cookie;
	}
	public Map<String, String> getReq_headers() {
		return req_headers;
	}
	public void setReq_headers(Map<String, String> req_headers) {
		this.req_headers = req_headers;
	}
	public String getReq_jsonbody() {
		return req_jsonbody;
	}
	public void setReq_jsonbody(String req_jsonbody) {
		this.req_jsonbody = req_jsonbody;
	}

	public int getResp_statuscode() {
		return resp_statuscode;
	}
	public void setResp_statuscode(int resp_statuscode) {
		this.resp_statuscode = resp_statuscode;
	}
	public Map<String, String> getResp_cookie() {
		return resp_cookie;
	}
	public void setResp_cookie(Map<String, String> resp_cookie) {
		this.resp_cookie = resp_cookie;
	}
	public Map<String, String> getResp_headers() {
		return resp_headers;
	}
	public void setResp_headers(Map<String, String> resp_headers) {
		this.resp_headers = resp_headers;
	}
	public String getResp_body() {
		return resp_body;
	}
	public void setResp_body(String resp_body) {
		this.resp_body = resp_body;
	}
	
}
